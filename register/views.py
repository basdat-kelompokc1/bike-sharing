from django.shortcuts import render
from django.contrib.auth import login
from .models import User
from django.views.generic import CreateView
from .forms import FormAdmin, FormAnggota, FormPetugas

# Create your views here.

def login_page(request):
    return render(request,'login_page.html')

def role_page(request):
    return render(request,'role_page.html')

def signup_petugas(request):
    return render(request,'signup_page.html')

def signup_anggota(request):
    return render(request,'signup_page.html')

class SignUpAdmin(CreateView):
    model = User
    form_class = FormAdmin
    template_name = 'signup_page.html'

    def get_context_data(self, **kwargs):
        kwargs['user_type'] = 'admin'
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('/')