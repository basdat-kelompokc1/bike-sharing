from django.urls import path
from . import views

app_name ="register"

urlpatterns = [
    path('signup/',views.role_page, name='role_page'),
    path('signup/admin/',views.SignUpAdmin.as_view(), name='signup_admin'),
    path('signup/petugas/',views.signup_petugas, name='signup_petugas'),
    path('signup/anggota/',views.signup_anggota, name='signup_anggota'),
    path('login/', views.login_page, name='login_page'),
]