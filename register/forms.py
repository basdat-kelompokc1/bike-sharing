from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.db import transaction
from .models import User, Admin, Petugas, Anggota

class FormAdmin(UserCreationForm):
  
    class Meta(UserCreationForm.Meta):
        model = User

    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        user.is_admin = True
        user.save()
        admin = Admin.objects.create(user=user)
        return user

class FormAnggota(UserCreationForm):
  
    class Meta(UserCreationForm.Meta):
        model = User

    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        user.is_anggota = True
        user.save()
        admin = Admin.objects.create(user=user)
        return user

class FormPetugas(UserCreationForm):
  
    class Meta(UserCreationForm.Meta):
        model = User

    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        user.is_petugas = True
        user.save()
        admin = Admin.objects.create(user=user)
        return user