from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.
class User(AbstractUser):
    is_admin = models.BooleanField(default=False)
    is_anggota = models.BooleanField(default=False)
    is_petugas = models.BooleanField(default=False)

class Admin(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    no_ktp = models.CharField(max_length=30)
    nama_lengkap = models.CharField(max_length=50)
    email = models.EmailField(max_length=50)
    tangal_lahir = models.DateField()
    nomor_telepon = models.CharField(max_length=20)
    alamat = models.TextField()

class Anggota(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    no_ktp = models.CharField(max_length=30)
    nama_lengkap = models.CharField(max_length=50)
    email = models.EmailField(max_length=50)
    tangal_lahir = models.DateField()
    nomor_telepon = models.CharField(max_length=20)
    alamat = models.TextField()

class Petugas(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    no_ktp = models.CharField(max_length=30)
    nama_lengkap = models.CharField(max_length=50)
    email = models.EmailField(max_length=50)
    tangal_lahir = models.DateField()
    nomor_telepon = models.CharField(max_length=20)
    alamat = models.TextField()