from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('daftar-sepeda', views.daftarSepeda, name='daftar_sepeda'),
    path('tambah-sepeda', views.tambahSepeda, name='tambah_sepeda'),
    
]