from django.shortcuts import render

# Create your views here.
def daftarSepeda(request):
	return render(request, 'daftar_sepeda.html')

def tambahSepeda(request):
	return render(request, 'tambah_sepeda.html')