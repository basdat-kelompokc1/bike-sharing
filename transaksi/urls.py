from django.urls import path
from . import views

app_name ="transaksi"

urlpatterns = [
    path('riwayat/',views.transaksi_page, name='transaksi_page'),
    path('topup/',views.topup_page, name='topup_page'),
    path('laporan/',views.laporan_page, name='laporan_page'),
]