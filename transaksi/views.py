from django.shortcuts import render

# Create your views here.
def transaksi_page(request):
    return render(request, 'transaksi_page.html')

def topup_page(request):
    return render(request, 'topup_page.html')

def laporan_page(request):
    return render(request, 'laporan_page.html')
