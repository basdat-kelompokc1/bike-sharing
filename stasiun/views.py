from django.shortcuts import render

# Create your views here.
def daftarStasiun(request):
	return render(request, 'daftar_stasiun.html')

def tambahStasiun(request):
	return render(request, 'tambah_stasiun.html')