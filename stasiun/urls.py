from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('daftar-stasiun', views.daftarStasiun, name='daftar_stasiun'),
    path('tambah-stasiun', views.tambahStasiun, name='tambah_stasiun'),
    
]